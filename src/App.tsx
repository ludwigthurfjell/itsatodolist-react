import React from "react";
import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import { RecoilRoot } from "recoil";

import Todos from "./pages/todos";
import Yolo from "./pages/yolo";

function App() {
  return (
    <BrowserRouter>
      <nav>
        <ol>
          <li>
            <Link to="/todos">TODO</Link>
          </li>
          <li>
            <Link to="/derp">Derp</Link>
          </li>
        </ol>
      </nav>
      <Switch>
        <RecoilRoot>
          <Route exact path="/todos">
            <Todos />
          </Route>
          <Route exact path="/derp">
            <Yolo />
          </Route>
        </RecoilRoot>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
