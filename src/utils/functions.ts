/**
 * Terrible name for a file
 */

export const lock = <T extends (...args: any[]) => any>(
  callback: T,
  waitFor: number
) => {
  let timeout: number;
  let lock = false;
  return (...args: Parameters<T>): ReturnType<T> => {
    let result: any;
    clearTimeout(timeout);

    if (!lock) {
      lock = true;
      result = callback(...args);
    }
    timeout = setTimeout(() => {
      lock = false;
    }, waitFor);
    return result;
  };
};
