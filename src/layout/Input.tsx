import React, { ForwardedRef, useRef, useState } from "react";

type Props = {
  id?: string;
  label: string;
  value: string;
  name?: string;
  error?: boolean;
  errorMessage?: string;
  required?: boolean;
  // onChange: (e: React.FormEvent<HTMLInputElement>) => void;
  onChange: ({ name, value }: { name?: string; value: string }) => void;
  onKeyDown?: (e: React.KeyboardEvent) => void;
  onFocus?: (e: React.SyntheticEvent) => void;
  onBlur?: (e: React.SyntheticEvent) => void;
  "data-testid"?: string;
};

const InputWithRef = React.forwardRef(
  (props: Props, ref: ForwardedRef<any>) => {
    const handleChange = (e: React.FormEvent<HTMLInputElement>) => {
      props.onChange({
        value: e.currentTarget.value,
        name: e.currentTarget.name,
      });
    };
    return <input {...props} onChange={handleChange} ref={ref} />;
  }
);

const Input = (props: Props) => {
  const {
    value,
    label,
    required,
    id,
    name,
    error,
    errorMessage,
    onChange,
    onKeyDown,
    onFocus,
    onBlur,
  } = props;
  const handleChange = (e: React.FormEvent<HTMLInputElement>) => {
    onChange({ name: e.currentTarget.name, value: e.currentTarget.value });
  };
  return (
    <span>
      <label htmlFor={id}>{label}</label>
      <input
        data-testid={props["data-testid"]}
        required={required}
        name={name}
        id={id}
        placeholder={label}
        value={value}
        onChange={handleChange}
        onKeyDown={onKeyDown}
        onFocus={onFocus}
        onBlur={onBlur}
      />
      {error && <small>{errorMessage}</small>}
    </span>
  );
};

Input.Select = (
  props: {
    options: string[];
    persist?: boolean;
    onSelect: (value: string) => void;
  } & Props
) => {
  const [showList, setShowList] = useState(false);
  const [value, setValue] = useState("");
  const inpRef = useRef<HTMLInputElement>();

  const handleChange = (d: { name?: string; value: string }) => {
    setValue(d.value);
  };

  const handleFocus = (e: React.SyntheticEvent) => {
    setShowList(true);
  };

  const handleOptionClick = (id: string) => (e: React.SyntheticEvent) => {
    e.preventDefault();
    setValue("");
    props.onSelect(id);

    if (!props.persist) {
      setShowList(false);
    }
    inpRef.current?.focus();
  };

  const handleKeyDown = (e: React.KeyboardEvent) => {
    if (e.key === "Enter") {
      handleOptionClick(value)(e);
    }
  };

  return (
    <div>
      <InputWithRef
        ref={inpRef}
        data-testid={props["data-testid"]}
        value={value}
        label={props.label}
        required={props.required}
        id={props.id}
        onChange={handleChange}
        onFocus={handleFocus}
        onKeyDown={handleKeyDown}
      />
      {showList && (
        <>
          {props.options
            .filter((o) => o.includes(value))
            .map((o) => (
              <button
                type="button"
                key={`tmp_${o}`}
                onClick={handleOptionClick(o)}
              >
                {o}
              </button>
            ))}
        </>
      )}
    </div>
  );
};
export default Input;
