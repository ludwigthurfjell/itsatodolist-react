import React from "react";

type Props = {
  id?: string;
  name?: string;
  label: string;
  value?: string;
  checked?: boolean;
  error?: boolean;
  errorMessage?: string;
  onChange: (e: React.FormEvent<HTMLInputElement>) => void;
  "data-testid"?: string;
};

const Checkbox = (props: Props) => {
  const { value, label, id, name, onChange, checked } = props;
  return (
    <span>
      <label htmlFor={id}>{label}</label>
      <input
        data-testid={props["data-testid"]}
        type="checkbox"
        name={name}
        id={id}
        placeholder={label}
        value={value}
        onChange={onChange}
        checked={checked}
      />
    </span>
  );
};

export default Checkbox;
