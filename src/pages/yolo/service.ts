import { apiUrls, storageKeys } from "./consts";

type Point = {
  x: number;
  y: number;
};

type Edge = {
  id: string;
  settlementAId: string;
  settlementBId: string;
  pointA: Point;
  pointB: Point;
};

type Settlement = {
  id: string;
  name: string;
  point: Point;
};

export default {
  getWorld: function getWorld(url: string = apiUrls.world): Promise<{
    edges: Edge[];
    settlements: Settlement[];
  }> {
    const edgesRaw = localStorage.getItem(storageKeys.edges);
    const settlementsRaw = localStorage.getItem(storageKeys.nodes);
    if (edgesRaw && settlementsRaw) {
      try {
        return new Promise((r) =>
          r({
            edges: JSON.parse(edgesRaw),
            settlements: JSON.parse(settlementsRaw),
          })
        );
      } catch (error) {
        console.log("hmm...");
        throw "hehe";
      }
    }

    return fetch(url)
      .then<{ edges: Edge[]; settlements: Settlement[] }>((r) => r.json())
      .then((data) => {
        localStorage.setItem(storageKeys.edges, JSON.stringify(data.edges));
        localStorage.setItem(
          storageKeys.nodes,
          JSON.stringify(data.settlements)
        );
        return data;
      })
      .catch((err) => {
        console.log(err);
        throw "yolo";
      });
  },
  login: function login(token: string, url: string = apiUrls.login) {
    fetch(url, {
      body: JSON.stringify({ token }),
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
      .then((r) => r.json())
      .then();
  },
};
