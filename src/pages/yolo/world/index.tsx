import React, { useEffect, useRef, useState } from "react";
import service from "../service";

const height = 1080;
const width = 1920;

function drawLandmark(
  size: number,
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  name: string
) {
  ctx.fillStyle = "black";
  ctx.fillRect(x - size / 2, y - size / 2, size, size);
  ctx.fillText(`${name} (${x},${y})`, x, y);
}

function drawLine(
  ctx: CanvasRenderingContext2D,
  fromX: number,
  fromY: number,
  toX: number,
  toY: number
) {
  ctx.beginPath();
  ctx.fillStyle = "blue";
  ctx.moveTo(fromX, fromY);
  ctx.lineTo(toX, toY);
  ctx.stroke();
}

export default function World() {
  const [isInit, init] = useState(false);
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const ctx = useRef<CanvasRenderingContext2D | null>();

  useEffect(() => {
    if (canvasRef.current && !isInit) {
      const context = (ctx.current = canvasRef.current.getContext("2d"));
      if (context) {
        service
          .getWorld()
          .then((data) => {
            console.log({ data });
            const context = ctx.current as CanvasRenderingContext2D;
            context.clearRect(0, 0, width, height);
            data.settlements.forEach((x) => {
              drawLandmark(10, context, x.point.x, x.point.y, x.name);
            });
            data.edges.forEach((e) => {
              drawLine(context, e.pointA.x, e.pointA.y, e.pointB.x, e.pointB.y);
            });
          })
          .catch((err) => console.log(err));
        init(true);
      }
    }
  }, [canvasRef, isInit]);

  useEffect(() => {}, [isInit]);

  return (
    <canvas
      //   onMouseDown={handleMouse}
      //   onMouseMove={handleMouseTrack}
      ref={canvasRef}
      width={width}
      height={height}
      id="yolo-canvas"
    ></canvas>
  );
}
