import React from "react";
import World from "./world";

function getMousePos(canvas: HTMLCanvasElement | null, evt: React.MouseEvent) {
  if (!canvas) return [];
  var rect = canvas.getBoundingClientRect(),
    scaleX = canvas.width / rect.width,
    scaleY = canvas.height / rect.height;

  return [
    (evt.clientX - rect.left) * scaleX,
    (evt.clientY - rect.top) * scaleY,
  ];
}

export default function Yolo() {
  return (
    <>
      <h1>Hejsan</h1>
      <World />
    </>
  );
}
