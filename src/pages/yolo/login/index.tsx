import React, { useState } from "react";
import Input from "../../../layout/Input";
import service from "../service";

export default function Login() {
  const [token, setToken] = useState("");
  const handleLogin = (e: React.FormEvent) => {
    e.preventDefault();
    service.login(token);
  };
  return (
    <form onSubmit={handleLogin}>
      <Input
        id="yolo.login"
        label="Login with your token"
        onChange={(e: React.FormEvent<HTMLInputElement>) =>
          setToken(e.currentTarget.value)
        }
        value={token}
      />
    </form>
  );
}
