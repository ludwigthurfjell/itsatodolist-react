export const storageKeys = {
  nodes: "yolo.nodes",
  edges: "yolo.edges",
};

export const apiUrls = {
  world: "http://localhost:8080/world",
  login: "http://localhost:8080/login",
};
