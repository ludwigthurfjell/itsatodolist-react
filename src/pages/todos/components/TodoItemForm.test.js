/**
 * @jest-environment jsdom
 */
import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import TodoItemForm from "./TodoItemForm";

const DESCRIPTION_INPUT_ID = "todos__todoitem__description";
const NAME_INPUT_ID = "todos__todoitem__name";
const TODO_TIME_TID = "todos__todoitem__time";
const TODO_ALIASES_ID = "todos__todoitem__aliases-inp";
const ADD = "Save";
const UPDATE = "Update";
const CANCEL = "Cancel";

describe("todos.TodoItemForm", () => {
  it("error display values", () => {
    render(
      <TodoItemForm
        todo={{
          name: { value: "name", errorMessage: "Name error" },
          description: { value: "desc", errorMessage: "Description error" },
          deadline: { value: "deadline", errorMessage: "Deadline error" },
          recurring: { value: true, errorMessage: "recurring " },
        }}
      />
    );
    expect(screen.getByText("Name error")).toBeInTheDocument();
    expect(screen.getByText("Description error")).toBeInTheDocument();
    expect(screen.getByText("Time error")).toBeInTheDocument();
  });

  it("error onErrorClear call on input change", () => {
    const fn = jest.fn();
    render(<TodoItemForm onErrorClear={fn} />);
    userEvent.type(screen.getByTestId(NAME_INPUT_ID), "123");
    expect(fn).toBeCalledTimes(3);
  });

  it("add enables save with required input values", () => {
    render(<TodoItemForm />);
    const addbtn = screen.getByText(ADD);
    const name = screen.getByTestId(NAME_INPUT_ID);
    userEvent.type(name, "hoho");
    expect(addbtn).not.toBeDisabled();
  });

  it("add disable save with space as value for required inputs", () => {
    render(<TodoItemForm />);
    const addbtn = screen.getByText(ADD);
    const name = screen.getByTestId(NAME_INPUT_ID);
    userEvent.type(name, "{space}");
    expect(addbtn).toBeDisabled();
  });

  it("onSubmit by enter press while focused in any input", () => {
    const cb = jest.fn();
    render(<TodoItemForm onSubmit={cb} />);
    const name = screen.getByTestId(NAME_INPUT_ID);
    const desc = screen.getByTestId(DESCRIPTION_INPUT_ID);
    const time = screen.getByTestId(TODO_TIME_TID);
    userEvent.type(name, "Title");
    userEvent.type(desc, "hej");
    userEvent.type(time, "2020-01-01{enter}");
    expect(cb).toHaveBeenCalledTimes(1);
  });

  it("add doesnt callback onUpadate by pressing enter without required fields while focused in any input", () => {
    const cb = jest.fn();
    render(<TodoItemForm onUpdate={cb} />);
    const desc = screen.getByTestId(DESCRIPTION_INPUT_ID);
    const time = screen.getByTestId(TODO_TIME_TID);
    userEvent.type(desc, "hej");
    userEvent.type(time, "10:00{enter}");
    expect(cb).toHaveBeenCalledTimes(0);
  });

  it("onSubmit by clicking add button", () => {
    const cb = jest.fn();
    render(<TodoItemForm onSubmit={cb} />);
    const addbtn = screen.getByText(ADD);
    const name = screen.getByTestId(NAME_INPUT_ID);
    const desc = screen.getByTestId(DESCRIPTION_INPUT_ID);
    userEvent.type(name, "Title");
    userEvent.type(desc, "hej");
    userEvent.click(addbtn, {}, { clickCount: 3 });
    expect(cb).toHaveBeenCalledTimes(1);
  });

  it("cancel callbacks onCancel by clicking cancel button", () => {
    const cb = jest.fn();
    render(<TodoItemForm onCancel={cb} />);
    const cancelBtn = screen.getByText(CANCEL);
    userEvent.click(cancelBtn);
    expect(cb).toBeCalled();
  });

  it("update sets input values from todo object props", () => {
    // render(
    //   <TodoItemForm
    //     todo={{
    //       name: { value: "title" },
    //       description: { value: "descs" },
    //       deadline: { value: "2020-01-01" },
    //       recurring: { value: false },
    //       aliases: { value: ["hoho"] },
    //       id: "123",
    //     }}
    //   />
    // );
    // const name = screen.getByTestId(NAME_INPUT_ID);
    // const desc = screen.getByTestId(DESCRIPTION_INPUT_ID);
    // const deadlineCb = screen.getByTestId("todos__todoitem__deadline-cb");
    // const deadline = screen.getByTestId(TODO_TIME_TID);
    // const aliases = screen.getByTestId(TODO_ALIASES_ID);
    // expect(name).toHaveValue("title");
    // expect(desc).toHaveValue("descs");
    // expect(deadline).toHaveValue("2020-01-01");
    // expect(updateBtn).toBeInTheDocument();
  });

  it("name from props", () => {
    render(
      <TodoItemForm
        todo={{
          name: { value: "Name" },
        }}
      />
    );
    expect(screen.getByTestId(NAME_INPUT_ID)).toHaveValue("Name");
  });

  it("description from props", () => {
    render(
      <TodoItemForm
        todo={{
          name: { value: "required" },
          description: { value: "Desc" },
        }}
      />
    );
    expect(screen.getByTestId(DESCRIPTION_INPUT_ID)).toHaveValue("Desc");
  });

  it("deadline from props", () => {
    render(
      <TodoItemForm
        todo={{
          name: { value: "required" },
          deadline: { value: "2020-01-01" },
        }}
      />
    );
    const deadlineCb = screen.getByTestId("todos__todoitem__deadline-cb");
    const deadline = screen.getByTestId(TODO_TIME_TID);

    expect(deadlineCb).toBeChecked();
    expect(deadline).toHaveValue("2020-01-01");
  });

  it("no deadline from incorrect format props", () => {
    render(
      <TodoItemForm
        todo={{
          name: { value: "required" },
          deadline: { value: "" },
        }}
      />
    );
    const deadlineCb = screen.getByTestId("todos__todoitem__deadline-cb");
    const deadline = screen.getByTestId(TODO_TIME_TID);

    expect(deadlineCb).not.toBeChecked();
    expect(deadline).toHaveValue("");
  });

  it("recurring true props", () => {});
  it("recurring false props", () => {});
  it("aliases empty props", () => {});
  it("aliases props", () => {});

  it("update disabled update button if a required field is removed", () => {
    render(
      <TodoItemForm
        todo={{
          name: "title",
          time: "10:00",
          description: "descs",
          id: "123",
        }}
      />
    );
    const name = screen.getByTestId(NAME_INPUT_ID);
    const updateBtn = screen.getByText(UPDATE);
    userEvent.type(name, "{selectall}{backspace}");

    expect(updateBtn).toBeDisabled();
  });

  it("update callbacks onUpdate with correct changed values", () => {
    const cb = jest.fn();
    render(
      <TodoItemForm
        todo={{
          name: { value: "title" },
          description: { value: "descs" },
          deadline: { value: "2021-01-01" },
          recurring: { value: false },
          aliases: { value: [] },
          id: "123",
        }}
        onSubmit={cb}
      />
    );
    const addbtn = screen.getByTestId("todos__todoitem__submit-btn");

    const name = screen.getByTestId(NAME_INPUT_ID);
    const desc = screen.getByTestId(DESCRIPTION_INPUT_ID);
    const deadlineCb = screen.getByTestId("todos__todoitem__deadline-cb");
    const deadline = screen.getByTestId(TODO_TIME_TID);
    const aliases = screen.getByTestId(TODO_ALIASES_ID);

    userEvent.type(name, "{backspace}3");
    userEvent.type(desc, " yolo");

    // userEvent.type(deadlineCb, "{space}");

    userEvent.type(deadline, "2022-01-01");
    userEvent.type(aliases, "test1{enter}test2{enter}");
    userEvent.click(addbtn);
    expect(cb).toBeCalledWith({
      name: { value: "titl3" },
      deadline: { value: "2022-01-01" },
      description: { value: "descs yolo" },
      aliases: { value: ["test1", "test2"] },
      recurring: { value: false },
      id: "123",
    });
  });
});
