import React from "react";

type Props = {
  name: string;
  id: string;
  onRemove: (id: string) => void;
  onEdit: (id: string) => void;
};

export default function TodoListItem(props: Props) {
  const { name, id, onRemove, onEdit } = props;
  const handleRemoveClick = (e: React.SyntheticEvent) => {
    e.preventDefault();
    onRemove(id);
  };

  const handleEditClick = (e: React.SyntheticEvent) => {
    e.preventDefault();
    onEdit(id);
  };

  return (
    <li>
      {name}
      <button
        data-testid="create__todoitemlist__edit-btn"
        onClick={handleEditClick}
      >
        E_ICON
      </button>
      <button
        data-testid="create__todoitemlist__remove-btn"
        onClick={handleRemoveClick}
      >
        R_ICON
      </button>
    </li>
  );
}
