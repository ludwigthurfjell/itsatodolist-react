import React, { useEffect, useMemo, useState } from "react";
import Checkbox from "../../../layout/Checkbox";
import Datepicker from "../../../layout/Datepicker";
import Input from "../../../layout/Input";

export enum TodoItemNameEnum {
  name = "name",
  description = "description",
  deadline = "deadline",
  recurring = "recurring",
}

export type TodoItemType = {
  id?: string;
  name: { value: string; errorMessage?: string };
  description?: { value: string; errorMessage?: string };
  deadline: { value: string; errorMessage?: string };
  recurring: { value: boolean; errorMessage?: string };
  aliases: { value: string[]; errorMessage?: string };
};

type Props = {
  onCancel?: () => void;
  onSubmit?: (item: TodoItemType) => void;
  todo?: TodoItemType;
};

export default function TodoItemForm(props: Props) {
  const [formData, setFormData] = useState<TodoItemType>({
    deadline: { value: "" },
    description: { value: "" },
    name: { value: "" },
    recurring: { value: false },
    aliases: { value: [] },
  });

  const [isDeadline, setIsDeadline] = useState(false);

  useEffect(() => {
    if (props.todo) {
      props.todo.deadline?.value && setIsDeadline(true);
      setFormData((d) => ({ ...d, ...props.todo }));
    }
  }, [props.todo]);

  // const handleInputChange = (e: React.FormEvent<HTMLInputElement>) => {
  const handleInputChange = (d: { value: string; name?: string }) => {
    const { value, name } = d;
    name && setFormData((d) => ({ ...d, [name]: { value } }));
  };

  const handleAliasChange = (option: string) => {
    setFormData((state) => ({
      ...state,
      aliases: { value: [...state.aliases.value, option] },
    }));
  };

  const handleCheckboxChange = (e: React.FormEvent<HTMLInputElement>) => {
    const { checked, name } = e.currentTarget;
    setFormData((d) => ({ ...d, [name]: checked }));
  };

  const handleSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
    props.onSubmit?.({
      ...formData,
      id: props.todo?.id,
    });
  };

  const handleCancelClick = (e: React.SyntheticEvent) => {
    e.preventDefault();
    props.onCancel?.();
  };

  return (
    <form data-testid="todo-section-add" onSubmit={handleSubmit}>
      <Input
        data-testid="todos__todoitem__name"
        required
        label="Name"
        value={formData.name.value}
        name="name"
        onChange={handleInputChange}
        error={!!props.todo?.name.errorMessage}
        errorMessage={props.todo?.name.errorMessage}
      />
      <Input
        data-testid="todos__todoitem__description"
        label="Description"
        name="description"
        onChange={handleInputChange}
        value={formData.description?.value || ""}
        error={!!props.todo?.description?.errorMessage}
        errorMessage={props.todo?.description?.errorMessage}
      />
      <section>
        <Checkbox
          data-testid="todos__todoitem__recurring"
          label="Recurring"
          id="recurring"
          checked={formData.recurring.value}
          onChange={handleCheckboxChange}
        />
      </section>

      <section>
        <Checkbox
          data-testid="todos__todoitem__deadline-cb"
          label="Deadline"
          name="deadline-cb"
          checked={isDeadline}
          onChange={function () {
            setIsDeadline(!isDeadline);
          }}
        />

        <Datepicker
          data-testid="todos__todoitem__time"
          name="deadline"
          disabled={!isDeadline}
          label="Deadline"
          onChange={handleInputChange}
          value={formData.deadline.value}
        />
      </section>

      <section>
        <ul>
          {formData.aliases.value.map((a) => (
            <li key={`fixme_${a}`}>{a}</li>
          ))}
        </ul>
        <Input.Select
          data-testid="todos__todoitem__aliases-inp"
          options={["qwe", "asd", "zxc", "rty"].filter(
            (x) => !formData.aliases.value.includes(x)
          )}
          name="aliases"
          label="Aliases"
          onChange={() => null}
          onSelect={handleAliasChange}
          value={""}
          persist
        />
      </section>
      <button
        data-testid="todos__todoitem__submit-btn"
        type="submit"
        disabled={!formData.name.value?.trim()}
      >
        {props.todo?.id ? "Update" : "Save"}
      </button>
      <button type="button" onClick={handleCancelClick}>
        Cancel
      </button>
    </form>
  );
}
