import React, { useState } from "react";
import TodoItemForm, { TodoItemType } from "./TodoItemForm";
import TodoListItem from "./TodoListItem";

type Props = {
  todos: TodoItemType[];
};

export default function Todos(props: Props) {
  const { todos } = props;
  const [stateTodos, setStateTodos] = useState();
  const [isAdding, setIsAdding] = useState(false);
  const [editId, setEditId] = useState("");

  const handleAddTodoItemClick = () => {
    setIsAdding(true);
  };

  const handleCancelTodoItem = () => {
    setIsAdding(false);
    setEditId("");
  };

  const handleClearTodoItemError = () => {};

  const handleRemoveItem = (id: string) => {};

  const handleToggleEditItem = (id: string) => {
    setEditId(id);
  };
  return (
    <>
      <button
        data-testid="todos__todoitem__add-btn"
        disabled={isAdding}
        onClick={handleAddTodoItemClick}
      >
        Add todo
      </button>
      {isAdding && <TodoItemForm />}
      {todos.map((todo) =>
        todo.id === editId ? (
          <TodoItemForm
            key={`edit_${todo.id}`}
            todo={todo}
            onSubmit={(item) => console.log("ITEM", item)}
            onCancel={handleCancelTodoItem}
          />
        ) : (
          <TodoListItem
            key={`listitem_${todo.id}`}
            id={String(todo.id)}
            name={todo.name.value}
            onRemove={handleRemoveItem}
            onEdit={handleToggleEditItem}
          />
        )
      )}
    </>
  );
}
