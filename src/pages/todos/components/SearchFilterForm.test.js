/**
 * @jest-environment jsdom
 */
import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import SearchFilterForm from "./SearchFilterForm";

describe("todos.SearchFilterForm", () => {
  it("renders default search input and create button", () => {
    render(<SearchFilterForm onSubmit={() => null} />);
    expect(screen.getByPlaceholderText("Search...")).toBeInTheDocument();
    expect(
      screen.getByText("Search", { selector: "button" })
    ).toBeInTheDocument();
  });

  it("input submit callbacks correct format, once on enter", () => {
    const cb = jest.fn();
    render(<SearchFilterForm onSubmit={cb} />);
    const searchInput = screen.getByPlaceholderText("Search...");
    userEvent.type(searchInput, "test");
    userEvent.keyboard("{enter}{enter}{enter}");

    expect(cb).toBeCalledTimes(1);
    expect(cb).toBeCalledWith({ textSearch: "test" });
  });

  it("search ignores empty search input submit", () => {
    const cb = jest.fn();
    render(<SearchFilterForm onSubmit={cb} />);
    const searchInput = screen.getByPlaceholderText("Search...");
    userEvent.type(searchInput, " ");
    userEvent.keyboard("{enter}");

    expect(cb).toBeCalledTimes(0);
  });

  it("button submit callbacks once", () => {
    const cb = jest.fn();
    render(<SearchFilterForm onSubmit={cb} />);
    const searchInput = screen.getByPlaceholderText("Search...");
    const submitButton = screen.getByText("Search", { selector: "button" });

    userEvent.type(searchInput, "test");
    userEvent.click(submitButton, {}, { clickCount: 3 });

    expect(cb).toBeCalledTimes(1);
    expect(cb).toBeCalledWith({ textSearch: "test" });
  });
});
