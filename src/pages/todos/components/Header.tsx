import React from "react";

type Props = {
  todoCount: number;
};

export default function Header(props: Props) {
  const { todoCount } = props;
  const header = todoCount
    ? `${todoCount} things to do today`
    : "Nothing to do today";
  return <h1>{header}</h1>;
}
