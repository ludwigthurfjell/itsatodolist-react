/**
 * @jest-environment jsdom
 */

import React from "react";
import { render, screen } from "@testing-library/react";
import Header from "./Header";

describe("todos.Header", () => {
  it("renders default header", () => {
    render(<Header />);
    expect(screen.getByText("Create a todo list!")).toBeInTheDocument();
  });

  it("renders correct header with hasTodos prop", () => {
    render(<Header hasTodos />);
    expect(screen.getByText("Todo lists")).toBeInTheDocument();
  });
});
