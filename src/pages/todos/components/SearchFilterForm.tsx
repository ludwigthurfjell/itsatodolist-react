import React, { useCallback, useState } from "react";
import { lock } from "../../../utils/functions";

type SubmitType = {
  textSearch: string;
};

type Props = {
  onSubmit: (data: SubmitType) => void;
};

type FormData = {
  textSearch: string;
};

export default function SearchFilterForm({ onSubmit }: Props) {
  const [formData, setFormData] = useState<FormData>({
    textSearch: "",
  });

  const submit = useCallback(
    lock((data) => onSubmit(data), 300),
    []
  );

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData((data) => ({ ...data, [name]: value }));
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (formData.textSearch.trim()) {
      submit(formData);
    }
  };

  return (
    <form role="form" onSubmit={handleSubmit}>
      <input
        name="textSearch"
        onChange={handleInputChange}
        value={formData.textSearch}
        placeholder="Search..."
      />
      <button type="submit">Search</button>
    </form>
  );
}
