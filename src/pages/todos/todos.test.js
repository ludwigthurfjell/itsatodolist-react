/**
 * @jest-environment jsdom
 */
import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import Todos from "./index";

describe("todos", () => {
  it("adding or editing a todo item disables add todo item button", () => {
    render(<Todos />);
    const addTodoButton = screen.getByTestId("todos__todoitem__add-btn");
    userEvent.click(addTodoButton);
    expect(addTodoButton).toBeDisabled();
  });

  it("adding todo item appends to todolist", () => {
    render(<Todos />);
    const addTodoButton = screen.getByTestId("todos__todoitem__add-btn");
    userEvent.click(addTodoButton);
    userEvent.type(screen.getByTestId("todos__todoitem__name"), "title{enter}");
    expect(screen.getByTestId("todos__create__todolist").children).toHaveLength(
      1
    );
  });

  it("adding todo item removes the new item form", () => {
    render(<Todos />);
    const addTodoButton = screen.getByTestId("todos__todoitem__add-btn");
    userEvent.click(addTodoButton);
    userEvent.type(screen.getByTestId("todos__todoitem__name"), "title{enter}");
    expect(screen.queryByTestId("todo-section-add")).not.toBeInTheDocument();
  });

  it("blocks adding todo item with same name", () => {
    render(<Todos />);
    const addTodoButton = screen.getByTestId("todos__todoitem__add-btn");
    userEvent.click(addTodoButton);
    userEvent.type(screen.getByTestId("todos__todoitem__name"), "title{enter}");
    userEvent.click(addTodoButton);
    userEvent.type(screen.getByTestId("todos__todoitem__name"), "title{enter}");
    expect(screen.getByTestId("todos__create__todolist").children).toHaveLength(
      1
    );
  });

  it("removing todo item removesfrom todolist", () => {
    render(<Todos />);
    const addTodoButton = screen.getByTestId("todos__todoitem__add-btn");
    userEvent.click(addTodoButton);
    userEvent.type(screen.getByTestId("todos__todoitem__name"), "title{enter}");
    userEvent.click(screen.getByTestId("create__todoitemlist__remove-btn"));
    expect(screen.getByTestId("todos__create__todolist").children).toHaveLength(
      0
    );
  });

  it("updates correct todo items on edit", () => {
    render(<Todos />);
    const addTodoButton = screen.getByTestId("todos__todoitem__add-btn");
    userEvent.click(addTodoButton);
    userEvent.type(screen.getByTestId("todos__todoitem__name"), "title{enter}");
    userEvent.click(screen.getByTestId("create__todoitemlist__edit-btn"));
    userEvent.type(screen.getByTestId("todos__todoitem__name"), "test2{tab}");
    userEvent.type(
      screen.getByTestId("todos__todoitem__description"),
      "d{tab}"
    );
    userEvent.type(screen.getByTestId("todos__todoitem__time"), "10:00{enter}");

    // TODO list item should possibly show some more info :D
    expect(screen.getAllByText("titletest2")).toHaveLength(1);
  });

  it("prevent saving same todo item name on edit", () => {
    render(<Todos />);
    userEvent.click(screen.getByTestId("todos__todoitem__add-btn"));
    userEvent.type(screen.getByTestId("todos__todoitem__name"), "test1{tab}");
    userEvent.type(
      screen.getByTestId("todos__todoitem__description"),
      "d{tab}"
    );
    userEvent.type(screen.getByTestId("todos__todoitem__time"), "10:00{enter}");

    userEvent.click(screen.getByTestId("todos__todoitem__add-btn"));
    userEvent.type(screen.getByTestId("todos__todoitem__name"), "test2{tab}");
    userEvent.type(
      screen.getByTestId("todos__todoitem__description"),
      "d{tab}"
    );
    userEvent.type(screen.getByTestId("todos__todoitem__time"), "10:00{enter}");

    userEvent.click(screen.getAllByTestId("create__todoitemlist__edit-btn")[0]);
    userEvent.type(
      screen.getByTestId("todos__todoitem__name"),
      "{selectall}{backspace}test2{enter}"
    );
    expect(screen.getByText("Name already used")).toBeInTheDocument();
  });

  it("type new group creates a new group in backend", () => {});
});
