import React, { useState } from "react";
import { Link } from "react-router-dom";
import Header from "./components/Header";
import Todos from "./components/Todos";
import SearchFilterForm from "./components/SearchFilterForm";
import { useRecoilValue } from "recoil";
import { todoMapState } from "./utils/recoil";

/**
 * route /todos
 */
export default function TodosPage() {
  const { todos, totalCount } = useRecoilValue(todoMapState);
  return (
    <>
      <Header todoCount={totalCount} />
      <SearchFilterForm onSubmit={(data) => console.log("Submit", { data })} />
      {/* We changed our minds here, let's not have lists but only todos with filterable group alias which acts as lists */}
      {/* <Link to="/create">Create new List</Link>
      <Lists lists={[]} /> */}
      <Todos todos={todos} />
    </>
  );
}
