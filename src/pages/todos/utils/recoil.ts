import { atom } from "recoil";
import { TodoItemType } from "../components/TodoItemForm";

export const todoKeys = {
  todoList: "todoListState",
};

export const todoMapState = atom<{ todos: TodoItemType[]; totalCount: number }>(
  {
    key: todoKeys.todoList,
    default: {
      todos: [
        {
          id: "123",
          name: { value: "Skojaren" },
          aliases: { value: ["Hej", "San"] },
          deadline: { value: "2021-03-05" },
          description: { value: "Yolo" },
          recurring: { value: true },
        },
      ],
      totalCount: 0,
    },
  }
);
